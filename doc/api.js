YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "Atom",
        "Bond",
        "Builder",
        "Conversion",
        "ForceField",
        "Molecule"
    ],
    "modules": [
        "openbabel"
    ],
    "allModules": [
        {
            "displayName": "openbabel",
            "name": "openbabel",
            "description": "The main module for binding Openbabel to Node.js"
        }
    ]
} };
});